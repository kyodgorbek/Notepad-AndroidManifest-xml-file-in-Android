# Notepad-AndroidManifest-xml-file-in-Android
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
         package="com.example.notepad"
>    <application android:icon="@drawable/app_notes"
           android:label="@string/app_name"
       >
             <provider android:name="NotePadProvider"
	        android:authorities="com.google.provider.NotePad"
	   />
	   <activity android:name="NotesList" android:label="@string/title_notes_list">
	     <intent-filter>
	           <action android:name="android.intent.action.MAIN" />
		   <category android:name="android.intent.category.Launcher" />
		</intent-filter>
		<intent-filter>
		 <action android:name="android.intent.action.VIEW" />
		 <action android:name="android.intent.action.PICK" />
		 <category android:name="android.intent.Default" />
		 <data android:mimiType="vnd.android.cursor.dir/vnd.google.note" />
		</activity>
...
</manifest>
